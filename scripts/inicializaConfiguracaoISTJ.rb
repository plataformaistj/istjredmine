
#Projetos para tribunais
$prjTribunais = []
$prjTribunais << {:name => "STF - Supremo Tribunal Federal", :identifier => "stf", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Supremo Tribunal Federal."}
$prjTribunais << {:name => "STJ - ", :identifier => "stj", :description => "Site para acompanhamento e uniformização das demandas de integração do Superior Tribunal de Justiça."}
$prjTribunais << {:name => "CJF", :identifier => "cfj", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Conselho de Justiça Federal."}
$prjTribunais << {:name => "TJAC - Tribunal de Justiça do Acre", :identifier => "tjac", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Acre."}
$prjTribunais << {:name => "TJAL - Tribunal de Justiça de Alagoas", :identifier => "tjal", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Alagoas."}
$prjTribunais << {:name => "TJAM - Tribunal de Justiça do Amazonas", :identifier => "tjam", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Amazonas."}
$prjTribunais << {:name => "TJAP - Tribunal de Justiça do Amapá", :identifier => "tjap", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Amapá."}
$prjTribunais << {:name => "TJBA - Tribunal de Justiça da Bahia", :identifier => "tjba", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça da Bahia."}
$prjTribunais << {:name => "TJCE - Tribunal de Justiça do Ceará", :identifier => "tjce", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Ceará."}
$prjTribunais << {:name => "TJDFT", :identifier => "tjdft", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Distrito Federal e Territórios."}
$prjTribunais << {:name => "TJES", :identifier => "tjes", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Espirito Santo."}
$prjTribunais << {:name => "TJGO", :identifier => "tjgo", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Goias."}
$prjTribunais << {:name => "TJMA", :identifier => "tjma", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Maranhão."}
$prjTribunais << {:name => "TJMG", :identifier => "tjmg", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Minas Gerais."}
$prjTribunais << {:name => "TJMS", :identifier => "tjms", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Mato Grosso do Sul."}
$prjTribunais << {:name => "TJMSP", :identifier => "tjmsp", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça Militar de São Paulo."}
$prjTribunais << {:name => "TJMT", :identifier => "tjmt", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Estado de Mato Grosso."}
$prjTribunais << {:name => "TJPA", :identifier => "tjpa", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Pará."}
$prjTribunais << {:name => "TJPB", :identifier => "tjpb", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça da Paraíba."}
$prjTribunais << {:name => "TJPE", :identifier => "tjpe", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Pernambuco."}
$prjTribunais << {:name => "TJPI", :identifier => "tjpi", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Piauí."}
$prjTribunais << {:name => "TJPR", :identifier => "tjpr", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Paraná."}
$prjTribunais << {:name => "TJRJ", :identifier => "tjrj", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Rio de Janeiro."}
$prjTribunais << {:name => "TJRN", :identifier => "tjrn", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Rio Grande do Norte."}
$prjTribunais << {:name => "TJRO", :identifier => "tjro", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Rondônia."}
$prjTribunais << {:name => "TJRR", :identifier => "tjrr", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Roraima."}
$prjTribunais << {:name => "TJRS", :identifier => "tjrs", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Rio Grande do Sul."}
$prjTribunais << {:name => "TJSC", :identifier => "tjsc", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Santa Catarina."}
$prjTribunais << {:name => "TJSE", :identifier => "tjse", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Sergipe."}
$prjTribunais << {:name => "TJSP", :identifier => "tjsp", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Estado de São Paulo - Avenida Brigadeiro(Direito Público)."}
$prjTribunais << {:name => "TJSPFJM", :identifier => "tjspfjm", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Estado de São Paulo - Fórum João Mendes (Criminal)."}
$prjTribunais << {:name => "TJSPPC", :identifier => "tjsppc", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Estado de São Paulo."}
$prjTribunais << {:name => "TJTO", :identifier => "tjto", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Tocantins."}
$prjTribunais << {:name => "TRF1", :identifier => "trf1", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Primeira Região."}
$prjTribunais << {:name => "TRF2", :identifier => "trf2", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Segunda Região."}
$prjTribunais << {:name => "TRF3", :identifier => "trf3", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Terceira Região."}
$prjTribunais << {:name => "TRF4", :identifier => "trf4", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Quarta Região."}
$prjTribunais << {:name => "TRF5", :identifier => "trf5", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Quinta Região."}

#Projetos para Entes Públicos
$prjEntesPublicos=[]
$prjEntesPublicos << {:name => "DPU", :identifier => "dpu", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com a Defensoria Pública da União."}
$prjEntesPublicos << {:name => "AGU", :identifier => "agu", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com a Advocacia Geral da União."}
$prjEntesPublicos << {:name => "MPU", :identifier => "mpu", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Ministério Público da União."}

$statusISTJ = []
$statusISTJ << {:name=> "Nova", :is_closed=> false}
$statusISTJ << {:name=> "Em execução", :is_closed=> false}
$statusISTJ << {:name=> "Cancelada", :is_closed=> true}
$statusISTJ << {:name=> "Suspensa", :is_closed=> false}
$statusISTJ << {:name=> "Solucionada", :is_closed=> true}
$statusISTJ << {:name=> "Fechada", :is_closed=> true}
$statusISTJ << {:name=> "Reaberta", :is_closed=> false}


inova = IssueStatus.all.find_by(name: "Nova")
if inova.nil?
    inova = {:name=>"Nova", :is_closed => false}
    inova = IssueStatus.create(inova)
    puts "criar #{inova[:name]}"
end
$trackerISTJ = []
$trackerISTJ << {:name=>"Implantação", :default_status_id => inova.id}
$trackerISTJ << {:name=>"Suporte TI", :default_status_id => inova.id}
$trackerISTJ << {:name=>"Corretiva Processo Judicial", :default_status_id => inova.id}
$trackerISTJ << {:name=>"Cadastro de Usuário", :default_status_id => inova.id}

$customFieldsISTJ = []
$customFieldsISTJ << { :name=> "Instituição", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {:text_formatting=>"", :url_pattern=>""}, :description=> ""}
$customFieldsISTJ << { :name=> "Lotação", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
$customFieldsISTJ << { :name=> "Cargo", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
$customFieldsISTJ << { :name=> "Área de atuação", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
$customFieldsISTJ << { :name=> "Telefone", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
$customFieldsISTJ << { :name=> "SIGA", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> false, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
$customFieldsISTJ << { :name=> "Marcador", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> false, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
puts $customFieldsISTJ
