
#puts $LOADED_FEATURES

pathRedmine = "/usr/src/redmine"
lib = pathRedmine 
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

lib = pathRedmine + "/app/controllers"
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

lib = pathRedmine + "/app/models"
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

lib = pathRedmine + "/app/helpers"
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

lib = pathRedmine + "/app/views"
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

dirLibISTJ = pathRedmine + "/scripts"
lib = File.expand_path(dirLibISTJ, __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

lib = pathRedmine 
require lib+'/config/application'
require 'rails/all'
puts ENV['RAILS_ENV']
puts $LOAD_PATH

