class ConfigurarProjetoISTJ
	def ConfigurarIssueCustomField
		# Criar campos customizados para tipos de tarefa de usuários
		customFields =[]
		customFields << { :name=> "Instituição", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {:text_formatting=>"", :url_pattern=>""}, :description=> ""}
		customFields << { :name=> "Lotação", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
		customFields << { :name=> "Cargo", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
		customFields << { :name=> "Área de atuação", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
		customFields << { :name=> "Telefone", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
		customFields << { :name=> "SIGA", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> false, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
		customFields << { :name=> "Marcador", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> false, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
		listaObj = customFields

		listaObj.each do |o|
		  obj = IssueCustomField.all.find_by_name(o[:name])
		  if obj.nil? 
			 obj = IssueCustomField.create(o)
			 puts "criar #{o[:name]}"
		  else
			 puts "Alterar #{o[:name]}"
			 obj.save
		  end
		end
	
	end

	
	def CriarProjects

		#Criar os tribunais
		puts "Projeto I-STJ"
		project = Project.new(name: "I-STJ", description: "Site para acompanhamento e uniformizacao das demandas de integração do STJ com os tribunais e as instituições (entes públicos, empresas privadas, órgãos, etc) que são conveniadas com os sistemas de integração.", parent: nil, identifier: "istj", inherit_members: true)
		project.enabled_module_names=["news", "documents", "wiki"]
		project.save


		puts "Projeto SATII"
		project = Project.new(name: "SATII", description: "Site operacional do SATII - Serviço de Atendimento aos Tribunais e Instituições Integragadas.", parent: Project.find("istj"), identifier: "satii", inherit_members: true)
		project.is_public = false
		project.enabled_module_names=["news", "documents", "wiki"]
		project.save

		puts "Projeto Modelo i-STJ"
		projectModelo = Project.new(name: "Modelo de Projeto", description: "Referência para criação de projetos", parent: Project.find("satii"), identifier: "istj-modelo", inherit_members: true)
		projectModelo.enabled_module_names=["issue_tracking", "news", "documents", "wiki", "boards", "agile"]
		projectModelo.inherit_members = false
		projectModelo.is_public = false
		projectModelo.trackers=Tracker.all  
		projectModelo.issue_custom_fields=IssueCustomField.where(name: ["SIGA"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Marcadores"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Instituição"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Lotacao"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Cargo"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Area de atuação"])  
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Telefone"])    
		projectModelo.save
		
		project = Project.new(name: "Tribunais", description: "Site para acompanhamento e uniformizacao das demandas de integração do STJ com os tribunais conveniados.", parent: Project.find("istj"), identifier: "tribunais", inherit_members: true)
		project.enabled_module_names=["news", "documents", "wiki"]
		project.save
		puts "Tribunais"

		tribunais = []
		tribunais << {:name => "STF - Supremo Tribunal Federal", :identifier => "stf", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Supremo Tribunal Federal."}
		tribunais << {:name => "STJ - ", :identifier => "stj", :description => "Site para acompanhamento e uniformização das demandas de integração do Superior Tribunal de Justiça."}
		tribunais << {:name => "CJF", :identifier => "cfj", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Conselho de Justiça Federal."}
		tribunais << {:name => "TJAC - Tribunal de Justiça do Acre", :identifier => "tjac", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Acre."}
		tribunais << {:name => "TJAL - Tribunal de Justiça de Alagoas", :identifier => "tjal", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Alagoas."}
		tribunais << {:name => "TJAM - Tribunal de Justiça do Amazonas", :identifier => "tjam", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Amazonas."}
		tribunais << {:name => "TJAP - Tribunal de Justiça do Amapá", :identifier => "tjap", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Amapá."}
		tribunais << {:name => "TJBA - Tribunal de Justiça da Bahia", :identifier => "tjba", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça da Bahia."}
		tribunais << {:name => "TJCE - Tribunal de Justiça do Ceará", :identifier => "tjce", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Ceará."}
		tribunais << {:name => "TJDFT", :identifier => "tjdft", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Distrito Federal e Territórios."}
		tribunais << {:name => "TJES", :identifier => "tjes", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Espirito Santo."}
		tribunais << {:name => "TJGO", :identifier => "tjgo", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Goias."}
		tribunais << {:name => "TJMA", :identifier => "tjma", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Maranhão."}
		tribunais << {:name => "TJMG", :identifier => "tjmg", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Minas Gerais."}
		tribunais << {:name => "TJMS", :identifier => "tjms", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Mato Grosso do Sul."}
		tribunais << {:name => "TJMSP", :identifier => "tjmsp", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça Militar de São Paulo."}
		tribunais << {:name => "TJMT", :identifier => "tjmt", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Estado de Mato Grosso."}
		tribunais << {:name => "TJPA", :identifier => "tjpa", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Pará."}
		tribunais << {:name => "TJPB", :identifier => "tjpb", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça da Paraíba."}
		tribunais << {:name => "TJPE", :identifier => "tjpe", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Pernambuco."}
		tribunais << {:name => "TJPI", :identifier => "tjpi", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Piauí."}
		tribunais << {:name => "TJPR", :identifier => "tjpr", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Paraná."}
		tribunais << {:name => "TJRJ", :identifier => "tjrj", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Rio de Janeiro."}
		tribunais << {:name => "TJRN", :identifier => "tjrn", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Rio Grande do Norte."}
		tribunais << {:name => "TJRO", :identifier => "tjro", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Rondônia."}
		tribunais << {:name => "TJRR", :identifier => "tjrr", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Roraima."}
		tribunais << {:name => "TJRS", :identifier => "tjrs", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Rio Grande do Sul."}
		tribunais << {:name => "TJSC", :identifier => "tjsc", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça de Santa Catarina."}
		tribunais << {:name => "TJSE", :identifier => "tjse", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Sergipe."}
		tribunais << {:name => "TJSP", :identifier => "tjsp", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Estado de São Paulo - Avenida Brigadeiro(Direito Público)."}
		tribunais << {:name => "TJSPFJM", :identifier => "tjspfjm", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Estado de São Paulo - Fórum João Mendes (Criminal)."}
		tribunais << {:name => "TJSPPC", :identifier => "tjsppc", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Estado de São Paulo."}
		tribunais << {:name => "TJTO", :identifier => "tjto", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal de Justiça do Tocantins."}
		tribunais << {:name => "TRF1", :identifier => "trf1", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Primeira Região."}
		tribunais << {:name => "TRF2", :identifier => "trf2", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Segunda Região."}
		tribunais << {:name => "TRF3", :identifier => "trf3", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Terceira Região."}
		tribunais << {:name => "TRF4", :identifier => "trf4", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Quarta Região."}
		tribunais << {:name => "TRF5", :identifier => "trf5", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Tribunal Regional Federal da Quinta Região."}


		projectModelo = Project.find_by_identifier("istj-modelo")
		tribunais.each do |t|
			project = Project.find_by_identifier(t[:identifier])
			if project.nil?
				project = Project.new(t)
				puts "Criar #{t[:description]}"
			else
				puts "Alterar #{t[:description]}"
			end
			project.parent = Project.find("tribunais")
			project.trackers = projectModelo.trackers
			project.issue_custom_fields = projectModelo.issue_custom_fields
			project.enabled_module_names=projectModelo.enabled_module_names
			project.inherit_members = projectModelo.inherit_members
			project.is_public = projectModelo.is_public
			project.save
		end

		project = Project.new(name: "Entes Públicos", description: "Site para acompanhamento e uniformizacao das demandas de integração do STJ com os entes públicos conveniados.", parent: Project.find("istj"), identifier: "entes-publicos", inherit_members: true)
		project.save

		entesPublicos=[]
		entesPublicos << {:name => "DPU", :identifier => "dpu", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com a Defensoria Pública da União."}
		entesPublicos << {:name => "AGU", :identifier => "agu", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com a Advocacia Geral da União."}
		entesPublicos << {:name => "MPU", :identifier => "mpu", :description => "Site para acompanhamento e uniformização das demandas de integração do STJ com o Ministério Público da União."}

		entesPublicos.each do |t|
		project = Project.find_by_identifier(t[:identifier])
		if project.nil?
			project = Project.new(t)
			puts "Criar #{t[:description]}"
		else
			puts "Alterar #{t[:description]}"  
		end
		project.parent = Project.find("entes-publicos")
		project.trackers = projectModelo.trackers
		project.issue_custom_fields = projectModelo.issue_custom_fields
		project.enabled_module_names=projectModelo.enabled_module_names
		project.inherit_members = projectModelo.inherit_members
		project.is_public = projectModelo.is_public
		project.save
		end
	end
	
	def ConfigurarStatus	

		#Criar status 
		status =[]
		status << {:name=> "Nova", :is_closed=> false}
		status << {:name=> "Em execução", :is_closed=> false}
		status << {:name=> "Cancelada", :is_closed=> true}
		status << {:name=> "Suspensa", :is_closed=> false}
		status << {:name=> "Solucionada", :is_closed=> true}
		status << {:name=> "Fechada", :is_closed=> true}
		status << {:name=> "Reaberta", :is_closed=> false}
		

		listaObj = status
		listaObj.each do |o|
		  obj = IssueStatus.all.find_by_name(o[:name])
		  if obj.nil?
			 obj = IssueStatus.create(o)
			 puts "criar #{o[:name]}"
		  else
			 puts "Alterar #{o[:name]}"
			 obj.is_closed = o[:is_closed]
			 obj.save
		  end
		end
	end


	def ConfigurarTrackers

		inova = IssueStatus.all.find_by(name: "Nova")

		tracker = []
		tracker << {:name=>"Implantação", :default_status_id => inova.id}
		tracker << {:name=>"Suporte TI", :default_status_id => inova.id}
		tracker << {:name=>"Corretiva Processo Judicial", :default_status_id => inova.id}
		tracker << {:name=>"Cadastro de Usuário", :default_status_id => inova.id}
		
		
		listaObj = tracker
		listaObj.each do |o|
		  obj = Tracker.all.find_by_name(o[:name])
		  if obj.nil?
			 obj = Tracker.create(o)
			 puts "criar #{o[:name]}"
			 obj.custom_fields << IssueCustomField.find_by_name("SIGA")
			 obj.custom_fields << IssueCustomField.find_by_name("Marcador")
		  else
			 puts "Alterar #{o[:name]}"
			 obj.default_status_id = o[:default_status_id]
			 obj.save
		  end
             obj.save
		end  
		  
		  trackerCadastroDeUsuario = Tracker.find_by_name("Cadastro de Usuário")
		  trackerCadastroDeUsuario.custom_fields   << IssueCustomField.find_by_name("Instituição")
		  trackerCadastroDeUsuario.custom_fields  << IssueCustomField.find_by_name("Lotação")
		  trackerCadastroDeUsuario.custom_fields << IssueCustomField.find_by_name("Cargo")
		  trackerCadastroDeUsuario.custom_fields << IssueCustomField.find_by_name("Área de atuação")
		  trackerCadastroDeUsuario.custom_fields << IssueCustomField.find_by_name("Telefone")
		  trackerCadastroDeUsuario.save
		  
		  
		  
		
	end
	

	def ConfigurarDefaultProject
		Setting["default_language"] = "pt-BR"
		Setting["sequential_project_identifiers"] = 0
		Setting["issue_group_assignment"] = 1
		Setting["cross_project_issue_relations"] = 1
		Setting["default_projects_modules"] = ["issue_tracking", "news", "documents", "wiki", "boards", "agile"]
		tracker_ids = Tracker.where(name: ["Implantação", "Suporte TI", "Corretiva Processo Judicial", "Cadastro de Usuário"]).map{|t|t.id.to_s}
		Setting["default_projects_tracker_ids"] =  tracker_ids
	end

	
	def ConfigurarRoles
		tracker_ids = Tracker.all.map{|t|t.id.to_s}
		role = Role.find_by_name("Negócio")
		if role.nil?
		   role = Role.new(name: "Negócio", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end
		role.permissions = []
		role.permissions = [:view_messages, :add_messages, :edit_own_messages]
		role.permissions << [:view_documents]
		role.permissions << [:manage_news, :comment_news]
		role.permissions << [:view_issues, :add_issues, :edit_issues, :copy_issues, :manage_issue_relations, :manage_subtasks, :add_issue_notes, :edit_own_issue_notes, :save_queries]
		role.permissions << [:view_issue_watchers, :add_issue_watchers, :delete_issue_watchers, :import_issues, :manage_categories]
		role.permissions << [:view_checklists, :done_checklists, :edit_checklists] 
		role.permissions << [:view_wiki_pages, :view_wiki_edits, :export_wiki_pages]
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"0", "edit_issues"=>"0", "add_issue_notes"=>"1", "delete_issues"=>"0"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=> tracker_ids, "edit_issues"=> tracker_ids, "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save

		#Criar role "TI"
		role = Role.find_by_name("TI")
		if role.nil?
		   role = Role.new(name: "TI", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end   
		role.permissions = Role.find_by_name("Negócio").permissions
		role.permissions << [:view_agile_queries]
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"0", "edit_issues"=>"0", "add_issue_notes"=>"1", "delete_issues"=>"0"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=> tracker_ids, "edit_issues"=> tracker_ids, "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save


		#Criar role "Negócio(STJ)"
		role = Role.find_by_name("Negócio(STJ)")
		if role.nil?
		   role = Role.new(name: "Negócio(STJ)", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end
		role.permissions = Role.find_by_name("TI").permissions
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"0", "edit_issues"=>"0", "add_issue_notes"=>"1", "delete_issues"=>"0"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=> tracker_ids, "edit_issues"=> tracker_ids, "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save

		#Criar role "TI(STJ)"
		role = Role.find_by_name("TI(STJ)")
		if role.nil?
		   role = Role.new(name: "TI(STJ)", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end
		role.permissions = Role.find_by_name("TI").permissions
		role.permissions << [:view_documents, :add_documents, :edit_documents, :delete_documents, :view_files, :manage_files]
		role.permissions << [:manage_public_agile_queries, :manage_agile_verions, :add_agile_queries, :view_agile_queries]
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"1", "edit_issues"=>"1", "add_issue_notes"=>"1", "delete_issues"=>"1"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=> tracker_ids, "edit_issues"=> tracker_ids, "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save

		#Criar role "TI.Atendimento(STJ)"
		role = Role.find_by_name("TI.Atendimento(STJ)")
		if role.nil?
		   role = Role.new(name: "TI.Atendimento(STJ)", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true) 
		end
		role.permissions = Role.find_by_name("TI(STJ)").permissions
		role.permissions << [:manage_news, :comment_news, :view_wiki_pages, :view_wiki_edits, :export_wiki_pages]
		role.permissions << [:edit_messages, :edit_own_messages, :delete_messages, :delete_own_messages]
		role.permissions << [:add_project, :edit_project, :select_project_modules, :manage_members, :manage_versions, :add_subprojects, :manage_public_queries, :save_queries]
		role.permissions << [:set_issues_private, :set_own_issues_private, :add_issue_notes, :edit_issue_notes, :edit_own_issue_notes, :view_private_notes, :set_notes_private, :delete_issues]
		role.permissions << [:manage_public_queries, :save_queries, :view_issue_watchers, :add_issue_watchers, :delete_issue_watchers, :import_issues, :manage_categories]
		role.permissions << [:view_checklists, :done_checklists, :edit_checklists, :manage_news, :comment_news, :view_wiki_pages]
		role.permissions << [:view_wiki_edits, :export_wiki_pages, :edit_wiki_pages, :rename_wiki_pages, :delete_wiki_pages, :delete_wiki_pages_attachments, :protect_wiki_pages, :manage_wiki]
		role.permissions << [:view_files, :manage_files]
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"1", "edit_issues"=>"1", "add_issue_notes"=>"1", "delete_issues"=>"1"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=>[], "edit_issues"=>[], "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save
	end

	
	def ConfigurarWorkFlows
		#recupera status para criacao de Workflows
		inova = IssueStatus.all.find_by(name: "Nova")
		iexec = IssueStatus.all.find_by(name: "Em execução")
		ican = IssueStatus.all.find_by(name: "Cancelada")
		isus = IssueStatus.all.find_by(name: "Suspensa")
		isol = IssueStatus.all.find_by(name: "Solucionada")
		ireab = IssueStatus.all.find_by(name: "Reaberta")
		ifecha = IssueStatus.all.find_by(name: "Fechada")

		puts inova 
		puts iexec 
		puts ican
		puts isus
		puts isol
		puts ireab 
		puts ifecha
		
		#cria workflows para role "TI"
		tracker = Tracker.find_by(name: "Suporte TI")
		role = Role.find_by(name: "TI")
		WorkflowTransition.where(role: role).delete_all
		WorkflowTransition.create(tracker: tracker, role: role, new_status: inova)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: inova, new_status: iexec)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: inova, new_status: ican)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: inova, new_status: isus)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: iexec, new_status: ican)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: iexec, new_status: isus)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: iexec, new_status: ifecha)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: ican, new_status: iexec)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: isus, new_status: iexec)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: isus, new_status: ican)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: isol, new_status: ireab)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: isol, new_status: ifecha)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: ireab, new_status: iexec)
		WorkflowTransition.copy(Tracker.find_by(name: "Suporte TI"), Role.find_by(name: "TI"), Tracker.all, Role.all)
	end

	def ConfigurarAll
	
		self.ConfigurarIssueCustomField
		self.ConfigurarStatus	
		self.CriarProjects
		self.ConfigurarTrackers
		self.ConfigurarDefaultProject
		self.ConfigurarRoles
		self.ConfigurarWorkFlows
	end
end
