class ConfigurarProjetoISTJ

	def ConfigurarIssueCustomField
		# Criar campos customizados para tipos de tarefa de usuários
		$customFieldsISTJ = []
	$customFieldsISTJ << { :name=> "Instituição", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {:text_formatting=>"", :url_pattern=>""}, :description=> ""}
	$customFieldsISTJ << { :name=> "Lotação", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
	$customFieldsISTJ << { :name=> "Cargo", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
	$customFieldsISTJ << { :name=> "Área de atuação", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
	$customFieldsISTJ << { :name=> "Telefone", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> true, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
	$customFieldsISTJ << { :name=> "SIGA", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> false, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
	$customFieldsISTJ << { :name=> "Marcador", :field_format=> "string", :possible_values=> nil, :regexp=> "", :min_length=> nil, :max_length=> nil, :is_required=> false, :is_for_all=> false, :is_filter=> false, :position=> 1, :searchable=> true, :default_value=> "", :editable=> true, :visible=> true, :multiple=> false, :format_store=> {"text_formatting"=>"", :"url_pattern"=>""}, :description=> ""}
		
		listaObj = $customFieldsISTJ

		listaObj.each do |o|
		  obj = IssueCustomField.all.find_by_name(o[:name])
		  if obj.nil?
			 obj = IssueCustomField.create(o)
			 puts "criar #{o[:name]}"
		  else
			 puts "Alterar #{o[:name]}"

		  end
			obj.save
		end

	end


	def CriarProjects

		#Criar os $prjTribunais
		puts "Projeto I-STJ"
		project = Project.new(name: "I-STJ", description: "Site para acompanhamento e uniformizacao das demandas de integração do STJ com os $prjTribunais e as instituições (entes públicos, empresas privadas, órgãos, etc) que são conveniadas com os sistemas de integração.", parent: nil, identifier: "istj", inherit_members: true)
		project.enabled_module_names=["news", "documents", "wiki"]
		project.save


		puts "Projeto SATII"
		project = Project.new(name: "SATII", description: "Site operacional do SATII - Serviço de Atendimento aos Tribunais e Instituições Integragadas.", parent: Project.find("istj"), identifier: "satii", inherit_members: true)
		project.is_public = false
		project.enabled_module_names=["news", "documents", "wiki"]
		project.save

		puts "Projeto Modelo i-STJ"
		projectModelo = Project.new(name: "Modelo de Projeto", description: "Referência para criação de projetos", parent: Project.find("satii"), identifier: "istj-modelo", inherit_members: true)
		projectModelo.enabled_module_names=["issue_tracking", "news", "documents", "wiki", "boards", "agile"]
		projectModelo.inherit_members = false
		projectModelo.is_public = false
		projectModelo.trackers=Tracker.all
		$customFieldsISTJ.each do |c|

			if not IssueCustomField.where(name: ["#{c[:name]}"]).nil?
				puts "Adicionando custom field #{c[:name]}"
				projectModelo.issue_custom_fields << IssueCustomField.where(name: ["#{c[:name]}"])
			end
		end

=begin
		projectModelo.issue_custom_fields=IssueCustomField.where(name: ["SIGA"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Marcadores"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Instituição"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Lotacao"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Cargo"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Area de atuação"])
		projectModelo.issue_custom_fields<<IssueCustomField.where(name: ["Telefone"])
=end

		projectModelo.save

		project = Project.new(name: "Tribunais", description: "Site para acompanhamento e uniformizacao das demandas de integração do STJ com os $prjTribunais conveniados.", parent: Project.find("istj"), identifier: "$prjTribunais", inherit_members: true)
		project.enabled_module_names=["news", "documents", "wiki"]
		project.save
		puts "Tribunais"



		projectModelo = Project.find_by_identifier("istj-modelo")
		puts $prjTribunais
		$prjTribunais.each do |t|
			project = Project.find_by_identifier(t[:identifier])
			if project.nil?
				project = Project.new(t)
				puts "Criar #{t[:description]}"
			else
				puts "Alterar #{t[:description]}"
			end

			project.parent = Project.find_by_identifier("tribunais")


			project.trackers = projectModelo.trackers
			project.issue_custom_fields = projectModelo.issue_custom_fields
			project.enabled_module_names=projectModelo.enabled_module_names
			project.inherit_members = projectModelo.inherit_members
			project.is_public = projectModelo.is_public
			project.save
		end

		project = Project.new(name: "Entes Públicos", description: "Site para acompanhamento e uniformizacao das demandas de integração do STJ com os entes públicos conveniados.", parent: Project.find("istj"), identifier: "entes-publicos", inherit_members: true)
		project.save


		$prjEntesPublicos.each do |t|
		project = Project.find_by_identifier(t[:identifier])
		if project.nil?
			project = Project.new(t)
			puts "Criar #{t[:description]}"
		else
			puts "Alterar #{t[:description]}"
		end
		project.parent = Project.find("entes-publicos")
		project.trackers = projectModelo.trackers
		project.issue_custom_fields = projectModelo.issue_custom_fields
		project.enabled_module_names=projectModelo.enabled_module_names
		project.inherit_members = projectModelo.inherit_members
		project.is_public = projectModelo.is_public
		project.save
		end
	end

	def ConfigurarStatus

		#Criar status
		puts "Configurando Status i-STJ"
		listaObj = $statusISTJ
		puts listaObj[1]
		obj = IssueStatus.new
		listaObj.each do |o|

		  obj = IssueStatus.all.find_by_name(o[:name])
		  if obj.nil?
			 obj = IssueStatus.create(o)
			 puts "criar #{o[:name]}"
		  else
			 puts "Alterar #{o[:name]}"
			 obj.is_closed = o[:is_closed]
			 obj.save
		  end
		end
	end


	def ConfigurarTrackers


		listaObj = $trackerISTJ
		listaObj.each do |o|
		  obj = Tracker.all.find_by_name(o[:name])
			puts obj
		  if obj.nil?
			 obj = Tracker.create(o)
			 puts "criar #{o[:name]}"
			 obj.custom_fields << IssueCustomField.find_by_name("SIGA")
			 obj.custom_fields << IssueCustomField.find_by_name("Marcador")
		  else
			 puts "Alterar o #{o[:name]}"
			 obj.default_status_id = o[:default_status_id]
		  end
			obj.save
		end

		aIssueCustomField = []
		aIssueCustomField << {:name=>"Instituição"}
		aIssueCustomField << {:name=>"Lotação"}
		aIssueCustomField << {:name=>"Cargo"}
		aIssueCustomField << {:name=>"Área de atuação"}
		aIssueCustomField << {:name=>"Telefone"}

   	trackerCadastroUsuario = Tracker.all.find_by_name("Cadastro de Usuário")
		listaObj = aIssueCustomField
		listaObj.each do |o|
			obj = trackerCadastroUsuario.custom_fields.find_by_name(o[:name])
		  if not obj.nil?
				trackerCadastroUsuario.custom_fields << obj
				puts "Inserindo IssueCustomField #{o[:name]}"
			end
		end
		trackerCadastroUsuario.save
	end


	def ConfigurarDefaultProject
		Setting["default_language"] = "pt-BR"
		Setting["sequential_project_identifiers"] = 0
		Setting["issue_group_assignment"] = 1
		Setting["cross_project_issue_relations"] = 1
		Setting["default_projects_modules"] = ["issue_tracking", "news", "documents", "wiki", "boards", "agile"]
		tracker_ids = Tracker.where(name: ["Implantação", "Suporte TI", "Corretiva Processo Judicial", "Cadastro de Usuário"]).map{|t|t.id.to_s}
		Setting["default_projects_tracker_ids"] =  tracker_ids
	end


	def ConfigurarRoles
		tracker_ids = Tracker.all.map{|t|t.id.to_s}
		role = Role.find_by_name("Negócio")
		if role.nil?
		   role = Role.new(name: "Negócio", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end
		role.permissions = []
		role.permissions = [:view_messages, :add_messages, :edit_own_messages]
		role.permissions << [:view_documents]
		role.permissions << [:manage_news, :comment_news]
		role.permissions << [:view_issues, :add_issues, :edit_issues, :copy_issues, :manage_issue_relations, :manage_subtasks, :add_issue_notes, :edit_own_issue_notes, :save_queries]
		role.permissions << [:view_issue_watchers, :add_issue_watchers, :delete_issue_watchers, :import_issues, :manage_categories]
		role.permissions << [:view_checklists, :done_checklists, :edit_checklists]
		role.permissions << [:view_wiki_pages, :view_wiki_edits, :export_wiki_pages]
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"0", "edit_issues"=>"0", "add_issue_notes"=>"1", "delete_issues"=>"0"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=> tracker_ids, "edit_issues"=> tracker_ids, "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save

		#Criar role "TI"
		role = Role.find_by_name("TI")
		if role.nil?
		   role = Role.new(name: "TI", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end
		role.permissions = Role.find_by_name("Negócio").permissions
		role.permissions << [:view_agile_queries]
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"0", "edit_issues"=>"0", "add_issue_notes"=>"1", "delete_issues"=>"0"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=> tracker_ids, "edit_issues"=> tracker_ids, "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save


		#Criar role "Negócio(STJ)"
		role = Role.find_by_name("Negócio(STJ)")
		if role.nil?
		   role = Role.new(name: "Negócio(STJ)", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end
		role.permissions = Role.find_by_name("TI").permissions
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"0", "edit_issues"=>"0", "add_issue_notes"=>"1", "delete_issues"=>"0"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=> tracker_ids, "edit_issues"=> tracker_ids, "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save

		#Criar role "TI(STJ)"
		role = Role.find_by_name("TI(STJ)")
		if role.nil?
		   role = Role.new(name: "TI(STJ)", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end
		role.permissions = Role.find_by_name("TI").permissions
		role.permissions << [:view_documents, :add_documents, :edit_documents, :delete_documents, :view_files, :manage_files]
		role.permissions << [:manage_public_agile_queries, :manage_agile_verions, :add_agile_queries, :view_agile_queries]
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"1", "edit_issues"=>"1", "add_issue_notes"=>"1", "delete_issues"=>"1"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=> tracker_ids, "edit_issues"=> tracker_ids, "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save

		#Criar role "TI.Atendimento(STJ)"
		role = Role.find_by_name("TI.Atendimento(STJ)")
		if role.nil?
		   role = Role.new(name: "TI.Atendimento(STJ)", assignable: true, issues_visibility: "all", users_visibility: "members_of_visible_projects", time_entries_visibility: "all", all_roles_managed: true)
		end
		role.permissions = Role.find_by_name("TI(STJ)").permissions
		role.permissions << [:manage_news, :comment_news, :view_wiki_pages, :view_wiki_edits, :export_wiki_pages]
		role.permissions << [:edit_messages, :edit_own_messages, :delete_messages, :delete_own_messages]
		role.permissions << [:add_project, :edit_project, :select_project_modules, :manage_members, :manage_versions, :add_subprojects, :manage_public_queries, :save_queries]
		role.permissions << [:set_issues_private, :set_own_issues_private, :add_issue_notes, :edit_issue_notes, :edit_own_issue_notes, :view_private_notes, :set_notes_private, :delete_issues]
		role.permissions << [:manage_public_queries, :save_queries, :view_issue_watchers, :add_issue_watchers, :delete_issue_watchers, :import_issues, :manage_categories]
		role.permissions << [:view_checklists, :done_checklists, :edit_checklists, :manage_news, :comment_news, :view_wiki_pages]
		role.permissions << [:view_wiki_edits, :export_wiki_pages, :edit_wiki_pages, :rename_wiki_pages, :delete_wiki_pages, :delete_wiki_pages_attachments, :protect_wiki_pages, :manage_wiki]
		role.permissions << [:view_files, :manage_files]
		role.permissions_all_trackers={"view_issues"=>"1", "add_issues"=>"1", "edit_issues"=>"1", "add_issue_notes"=>"1", "delete_issues"=>"1"}
		role.permissions_tracker_ids={"view_issues"=>[], "add_issues"=>[], "edit_issues"=>[], "add_issue_notes"=>[], "delete_issues"=>[]}
		role.save
	end


	def ConfigurarWorkFlows
		#recupera status para criacao de Workflows
		inova = IssueStatus.all.find_by(name: "Nova")
		iexec = IssueStatus.all.find_by(name: "Em execução")
		ican = IssueStatus.all.find_by(name: "Cancelada")
		isus = IssueStatus.all.find_by(name: "Suspensa")
		isol = IssueStatus.all.find_by(name: "Solucionada")
		ireab = IssueStatus.all.find_by(name: "Reaberta")
		ifecha = IssueStatus.all.find_by(name: "Fechada")

		puts inova
		puts iexec
		puts ican
		puts isus
		puts isol
		puts ireab
		puts ifecha

		#cria workflows para role "TI"
		tracker = Tracker.find_by(name: "Suporte TI")
		role = Role.find_by(name: "TI")
		WorkflowTransition.where(role: role).delete_all
		WorkflowTransition.create(tracker: tracker, role: role, new_status: inova)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: inova, new_status: iexec)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: inova, new_status: ican)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: inova, new_status: isus)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: iexec, new_status: ican)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: iexec, new_status: isus)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: iexec, new_status: ifecha)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: ican, new_status: iexec)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: isus, new_status: iexec)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: isus, new_status: ican)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: isol, new_status: ireab)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: isol, new_status: ifecha)
		WorkflowTransition.create(tracker: tracker, role: role, old_status: ireab, new_status: iexec)
		WorkflowTransition.copy(Tracker.find_by(name: "Suporte TI"), Role.find_by(name: "TI"), Tracker.all, Role.all)
	end

	def ConfigurarAll
		load ('inicializaConfiguracaoISTJ.rb')
		self.ConfigurarIssueCustomField
		self.ConfigurarStatus
		self.CriarProjects
		self.ConfigurarTrackers
		self.ConfigurarDefaultProject
		self.ConfigurarRoles
		self.ConfigurarWorkFlows
		puts 'Configurado'
	end
end

load ('inicializaConfiguracaoISTJ.rb')
puts "TUDO CERTO!!!"
conf = ConfigurarProjetoISTJ.new
conf.ConfigurarAll
puts "MAIS CERTO AINDA!!"